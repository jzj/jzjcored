/* Bit extention functions (Sign and zero extention) */

//Generic functions

function automatic [31:0] signExtend8To32(input [7:0] dataIn);
begin
	signExtend8To32 = {{24{dataIn[7]}}, dataIn};
end
endfunction

function automatic [31:0] zeroExtend8To32(input [7:0] dataIn);
begin
	zeroExtend8To32 = {24'h000000, dataIn};
end
endfunction

function automatic [31:0] signExtend16To32(input [15:0] dataIn);
begin
	signExtend16To32 = {{16{dataIn[15]}}, dataIn};
end
endfunction

function automatic [31:0] zeroExtend16To32(input [15:0] dataIn);
begin
	zeroExtend16To32 = {16'h0000, dataIn};
end
endfunction

//Function that will probably only be used for instruction decoding

function automatic [31:0] signExtend12To32(input [11:0] dataIn);
begin
	signExtend12To32 = {{20{dataIn[11]}}, dataIn};
end
endfunction

function automatic [31:0] signExtend13To32(input [12:0] dataIn);
begin
	signExtend13To32 = {{19{dataIn[12]}}, dataIn};
end
endfunction

function automatic [31:0] signExtend21To32(input [20:0] dataIn);
begin
	signExtend21To32 = {{11{dataIn[20]}}, dataIn};
end
endfunction